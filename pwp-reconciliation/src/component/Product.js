import React, { Component } from 'react';
import ProductModal from './ProductModal';
import Spinner from './Spinner';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { productPopup } from '../action/index';
import { Grid, Row, Col, Glyphicon, Image, Pager, Tooltip, OverlayTrigger} from 'react-bootstrap';

class Product extends Component{
    constructor(){
        super()
        this.state={products:[],nextPage:false,prevPage:false,offset:0,spinner:true}
        this.onClick=this.onClick.bind(this);
        this.onClickPagination=this.onClickPagination.bind(this);
    }

    onClickPagination(e){
        console.log(e.target.text);
        switch(e.target.text){
            case 'Previous':
            if(this.state.prevPage){
                this.setState({offset:this.state.offset-80})
            }
            break;
            case 'Next':
            if(this.state.nextPage){
                this.setState({offset:this.state.offset+80})
            }
            break;
        }
    }

    onClick(item){
       this.props.productPopup(item,true);
    }

    componentWillMount(){      
        if(this.props.catNavStatus===1){
        fetch('http://35.200.230.50/productcatalog/v1/libraryproduct/?limit=80')
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            console.log(data,'product data');
            if(data.message===undefined){
            this.setState({products:data.result,spinner:false,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage
            });
            }
        })
        .catch(error=>{
            console.log(error);
        })
        }
    }

    componentDidUpdate(prevProps,prevState){
        let startPoint;
        console.log(this.props.filter.status)
        if(this.props.filter.name!==prevProps.filter.name||this.props.filter.status!==prevProps.filter.status
        ||this.props.filter.id!==prevProps.filter.id||this.props.catNavStatus!==prevProps.catNavStatus
        ||prevState.offset!==this.state.offset){
            
        if(prevState.offset!==this.state.offset){
            startPoint=this.state.offset
        }
        else{
            startPoint=0
        }
        this.setState({offset:startPoint,spinner:true},function(){    
        if(this.props.catNavStatus===1){
            console.log(this.props.filter.status)
                if(this.props.filter.name===''){
                    if(this.props.filter.status==='Status'){
                        fetch(`http://35.200.230.50/productcatalog/v1/libraryproduct/?limit=60&&offset=${this.state.offset}`)
                        .then(response=>{
                            return response.json()
                        })
                        .then(data=>{
                            if(data.message===undefined){
                            this.setState({products:data.result,nextPage:data.hasNextPage,
                                prevPage:data.hasPreviousPage,spinner:false})
                            }
                        })
                        .catch(e=>{
                            console.log(e)
                        })
                    }
                    else{
                        fetch(`http://35.200.230.50/productcatalog/v1/libraryproduct/?limit=60&&status=${this.props.filter.status}&&offset=${this.state.offset}`)
                        .then(response=>{
                            return response.json()
                        })
                        .then(data=>{
                            if(data.message===undefined){
                            this.setState({products:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false})
                            }
                        })
                        .catch(e=>{
                            console.log(e)
                        })
                    }
                }
                else{
                    if(this.props.filter.status==='Status'){
                        fetch(`http://35.200.230.50/productcatalog/v1/libraryproduct/?searchText=${this.props.filter.name}&&limit=60&&offset=${this.state.offset}`)
                        .then(response=>{
                            return response.json()
                        })
                        .then(data=>{
                            if(data.message===undefined){
                            this.setState({products:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false})
                            }
                        })
                        .catch(e=>{
                            console.log(e)
                        })
                    }
                    else{
                        fetch(`http://35.200.230.50/productcatalog/v1/libraryproduct/?searchText=${this.props.filter.name}&&status=${this.props.filter.status}&&limit=60&&offset=${this.state.offset}`)
                        .then(response=>{
                            return response.json()
                        })
                        .then(data=>{
                            if(data.message===undefined){
                            this.setState({products:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false})
                            }
                        })
                        .catch(e=>{
                            console.log(e)
                        })
                    }
                }
        }
        else{
            console.log('catNAvStatus==2')
               if(this.props.filter.id==='1'){
                        if(this.props.filter.status==='Status'){
                            if(this.props.filter.name===''){
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/23416/product/?limit=60&&offset=${this.state.offset}`)
                                .then(response=>{
                                    return response.json();
                                })
                                .then(data=>{
                                    console.log(data.result)
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e);
                                })
                            }
                            else{
                                console.log('catNavStatus==2 and name')
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/23416/product/?limit=60&&searchText=${this.props.filter.name}&&offset=${this.state.offset}`)
                                .then(response=>{
                                    return response.json();
                                })
                                .then(data=>{
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e);
                                })
                            }
                        }
                        else{
                            if(this.props.filter.name===''){
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/23416/product/?limit=60&&status=${this.props.filter.status}&&offset=${this.state.offset}`)
                                .then(response=>{
                                    return response.json()
                                })
                                .then(data=>{
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e)
                                })
                            }
                            else{
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/23416/product/?limit=60&&searchText=${this.props.filter.name}&&status=${this.props.filter.status}&&offset=${this.state.offset}`)
                                .then(response=>{
                                    return response.json()
                                })
                                .then(data=>{
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e)
                                })
                            }
                        }
                    }
                    else{
                        if(this.props.filter.status==='Status'){
                            if(this.props.filter.name===''){
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/${this.props.filter.id}/product/?limit=60&&offset${this.state.offset}`)
                                .then(response=>{
                                    return response.json()
                                })
                                .then(data=>{
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e)
                                })
                            }
                            else{
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/${this.props.filter.id}/product/?limit=60&&searchText=${this.props.filter.name}offset${this.state.offset}`)
                                .then(response=>{
                                    return response.json()
                                })
                                .then(data=>{
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e)
                                })
                            }
                        }
                        else{
                            if(this.props.filter.name===''){
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/${this.props.filter.id}/product/?limit=60&&status=${this.props.filter.status}offset${this.state.offset}`)
                                .then(response=>{
                                    return response.json()
                                })
                                .then(data=>{
                                    console.log(data)
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e)
                                })
                            }
                            else{
                                fetch(`http://35.200.230.50/productcatalog/v1/retailer/${this.props.filter.id}/product/?limit=60&&status=${this.props.filter.status}&&searchText=${this.props.filter.name}offset${this.state.offset}`)
                                .then(response=>{
                                    return response.json()
                                })
                                .then(data=>{
                                    if(data.message===undefined){
                                    this.setState({products:data.result,nextPage:data.result.hasNextPage,prevPage:data.result.hasPreviousPage,spinner:false})
                                    }
                                })
                                .catch(e=>{
                                    console.log(e)
                                })
                            }
                        }
                    }
            }
        }) 
        }
    }



    render(){
        console.log(this.state);
        const tooltipleft = (
            <Tooltip id="tooltip">
              <strong>Newer</strong>
            </Tooltip>
          );
        const tooltipright =(
            <Tooltip id="tooltip">
              <strong>Older</strong>
            </Tooltip>
        )
        const list=this.state.products.map(item=>{
            let component;
            if(item.approvalInfo.status==='PENDING'){
                component=<h4>Not Verified</h4>
            }
            else{
                component=<div>Verified<span><Glyphicon glyph='ok' className='verification-status'></Glyphicon></span></div>
            }
          return <Grid fluid className='product-details' key={item._id}>
                 <Row>
                     <Col lg={3}><Image src={item.imageUrls[0]} responsive className='product-image'></Image></Col>
                     <Col lg={9}>
                     <div className='product-name'>
                        <Row><Col>{item.name}</Col></Row>
                        <Row><Col className='nickname'>{item.nickname}</Col></Row>
                     </div>
                        <Row><Col className='product-description'>{item.description}</Col></Row>
                        <Row>
                            <Col lg={6} className='column'>{component}</Col>
                            <Col lg={6} className='column text-right'><Glyphicon glyph='pencil'className='edit-product' onClick={()=>
                                this.onClick(item)}>Edit</Glyphicon></Col>
                        </Row>
                     </Col>
                 </Row>
             </Grid>
        });
        return(
            <div>
             <div className='product-pagination'>
             <Pager>
                    <OverlayTrigger placement="top" overlay={tooltipleft} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.prevPage}>Previous</Pager.Item></OverlayTrigger>{' '}
                    <OverlayTrigger placement='top' overlay={tooltipright} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.nextPage}>Next</Pager.Item></OverlayTrigger>
            </Pager>
             </div>   
            <div className='product-showcase'>
                  {this.state.spinner?<Spinner/>:this.state.products.length===0?<h2 className='no-match-found-product'>No result found!..</h2>:list}
                 <ProductModal/>
            </div>
            </div>
        );
    }
}
const mapStateToProps=(state)=>{
    return({
        filter:state.productReducer,
        catNavStatus:state.navigationReducer.catNav
    })
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({productPopup},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Product);