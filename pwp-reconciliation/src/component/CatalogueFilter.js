import React, { Component } from 'react';
import { connect } from 'react-redux';
import SelectSearch from 'react-select-search';
import { Grid, Row, Col, FormGroup, FormControl, InputGroup, Glyphicon} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { productFilter } from '../action/index';

class CatalogueFilter extends Component{
    constructor(){
        super()
        this.state={name:'',status:'Status',retailers:[],id:'1'}
        this.onSelectProduct=this.onSelectProduct.bind(this);
        this.onSelectStatus=this.onSelectStatus.bind(this);
        this.onSelectRetailer=this.onSelectRetailer.bind(this);
    }

    onSelectProduct(e){
        console.log(e)
        this.setState({name:e.target.value},function(){
            this.props.productFilter(this.state);
        });
    }

    onSelectStatus(e){
        console.log(e)
        this.setState({status:e.target.value},function(){
            this.props.productFilter(this.state)
        })
    }

    onSelectRetailer(e){
        this.setState({id:e.value},function(){
            this.props.productFilter(this.state)
        })
    }

    componentWillMount(){
        fetch('http://saggregation.mpaani.com/pwpreconciliation/v1/eligibleretailer/')
        .then(response=>{
            return(response.json())
        })
        .then(data=>{
            console.log(data.result)
            if(data.message===undefined){
            this.setState({retailers:data.result})
            }
        })
        .catch(e=>{
            console.log(e);
        })
    }

    componentDidUpdate(prevProps){
        if(prevProps.catNavStatus!==this.props.catNavStatus){
            this.setState({name:'',id:'1',status:'Status'},function(){
                this.props.productFilter(this.state);
            })
        }
    }

    render(){
        let options=[{value:'1',name:'Choose Retailer'}];
        if(this.state.retailers.length!==0){
        this.state.retailers.forEach(item=>{
            options.push({value:item.retailerCode,name:item.retailerName})
        })
        }
        return(
            <Grid fluid className='catalogue-filter'>
               <Row className='catalogue-filter-row'>
                   <Col lg={2} md={2}>
                     <FormControl type='text' className='select-product' placeholder='search' onChange={this.onSelectProduct} value={this.state.name}></FormControl>
                   </Col>
                   <Col lg={2} md={2}>
                   <form className='select-form'>
                          <FormGroup controlId='formControlsSelect'>
                              <FormControl componentClass='select' placeholder='Status' onChange={this.onSelectStatus} value={this.state.status}>
                                  <option value='Status'>Status</option>
                                  <option value='PENDING'>Not Verified</option>
                                  <option value='APPROVED'>Verified</option>
                            </FormControl>
                          </FormGroup>
                      </form>
                   </Col>
                   <Col lg={2} lg={2}>
                   {this.props.catNavStatus===2?<SelectSearch options={options} value={this.state.id} onChange={this.onSelectRetailer}/>:''}
                   </Col>
               </Row>
            </Grid>
        );
    }
}

const mapStateToProps=(state)=>{
    return({
        catNavStatus:state.navigationReducer.catNav
    })
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({productFilter},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(CatalogueFilter);