import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Glyphicon, Table,Pager, OverlayTrigger, Tooltip} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { retailerPendingTransactionFilter,reconciliationNavigation } from '../action/index';
import Spinner from './Spinner';

class Retailer extends Component{
    constructor()
    {
        super()
        this.state={id:'1',retailers:[],spinner:true,nextPage:false,prevPage:false,offset:0};
        this.onClick=this.onClick.bind(this);
        this.onClickPagination=this.onClickPagination.bind(this);
    }

    onClick(id){
        this.props.reconciliationNavigation({select:2});  
        this.props.retailerPendingTransactionFilter(id)
    }

    onClickPagination(e){
        switch(e.target.text){
            case 'Previous':
            if(this.state.prevPage){
                this.setState({offset:this.state.offset-50})
            }
            break;
            case 'Next':
            if(this.state.nextPage){
                this.setState({offset:this.state.offset+50})
            }
            break;
        }
    }

    componentWillMount(){
        fetch('http://saggregation.mpaani.com/pwpreconciliation/v1/eligibleretailer/?limit=50')
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            if(data.message===undefined){
            this.setState({retailers:data.result,spinner:false,nextPage:data.hasNextPage,
                prevPage:data.hasPreviousPage})
            }    
        })
        .catch(e=>{
            console.log(e);
        })
    }
    
    componentDidUpdate(prevProps,prevState){
        if(this.props.filter!==this.state.id||prevState.offset!==this.state.offset)
        {
            this.setState({id:this.props.filter,spinner:true,offset:prevProps.filter!==this.props.filter?0:this.state.offset,retailers:[]},function(){
                if(this.state.id==='1')
                {
                    fetch(`http://saggregation.mpaani.com/pwpreconciliation/v1/eligibleretailer/?limit=50&&offset=${this.state.offset}`)
                    .then(response=>{
                        return response.json()
                    })
                    .then(data=>{
                        if(data.message===undefined){
                        this.setState({retailers:data.result,spinner:false,nextPage:data.hasNextPage,
                        prevPage:data.PreviousPage})
                        }
                    })
                    .catch(e=>{
                        console.log(e);
                    })
                }
                else
                {
                    console.log('data');
                    fetch(`http://saggregation.mpaani.com/pwpreconciliation/v1/eligibleretailer/?retailerCode=${this.state.id}&&limit=50&&offset=${this.state.offset}`)
                    .then(response=>{
                        return response.json()
                    })
                    .then(data=>{
                        if(data.message===undefined){
                        this.setState({retailers:data.result,spinner:false,nextPage:data.hasNextPage,
                        prevPage:data.PreviousPage})
                        }
                    })
                    .catch(e=>{
                        console.log(e)
                    })
                }
            })
        }
    }

    render(){
        console.log(this.state)
        let retailerList=[];
        let list=[];
        list=this.state.retailers.sort(function(a,b){
            if(a.retailerName<b.retailerName)
            return -1;
            if(a.retailerName>b.retailerName)
            return 1;
            return 0;
        })
        retailerList=list.map(item=>{
            return<tr key={item.retailerCode}>
                <td className='retailer-retailers'>{item.retailerName}</td>
                <td className='retailer-code'>{item.retailerCode}</td>
                <td className='retailer-phone'>{item.phoneNumber}</td>
                <td className='retailer-transaction'>{item.transactions}</td>
                <td className='retailer-amount'>{item.pendingAmount}</td>
                <td className='retailer-transition'><Glyphicon glyph='folder-open'onClick={()=>this.onClick(item.retailerCode)}><span className='tooltiptext'>{item.retailerName} Pending Transactions</span></Glyphicon></td>
            </tr>
        })

        const tooltipleft = (
            <Tooltip id="tooltip">
              <strong>Newer</strong>
            </Tooltip>
          );
        const tooltipright =(
            <Tooltip id="tooltip">
              <strong>Older</strong>
            </Tooltip>
        ) 
        return(
            <div>
            <Grid fluid>
            <Row className='total'>
                <Col lg={4} md={4}></Col>
                <Col lg={4} md={4} className='pagination'>
                <Pager>
                 <OverlayTrigger placement="top" overlay={tooltipleft} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.prevPage}>Previous</Pager.Item></OverlayTrigger>{' '}
                 <OverlayTrigger placement='top' overlay={tooltipright} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.nextPage}>Next</Pager.Item></OverlayTrigger>
                </Pager>
                </Col>
                <Col lg={4} md={4} ></Col>
            </Row>
         </Grid>
            <Table hover condensed striped responsive>
                <thead className='table-heading'>
                    <tr>
                        <th className='retailer-retailers'>Retailer</th>
                        <th className='retailer-code'>Code</th>
                        <th className='retailer-phone'>Phone Number</th>
                        <th className='retailer-transaction'>Transaction</th>
                        <th className='retailer-amount'>Amount</th>
                        <th className='retailer-transition'></th>
                    </tr>
                </thead>
                    {this.state.spinner||this.state.retailers.length===0?<tbody></tbody>:<tbody>{retailerList}</tbody>}
            </Table>
            {this.state.spinner?<Spinner/>:this.state.retailers.length===0?<h2 className='no-match-found'>No result found!...</h2>:''}
         </div>
        );
        }
    }

const mapStateToProps=(state)=>{
    return({
        filter:state.retailerReducer.id
    })
}
const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({retailerPendingTransactionFilter,reconciliationNavigation},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Retailer);