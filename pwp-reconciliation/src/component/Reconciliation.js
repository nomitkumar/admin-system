import React, { Component } from 'react';
import Transaction from './Transaction';
import Payment from './Payment';
import Retailer from './Retailer';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';

class Reconciliation extends Component{

    render(){
        let component;
        switch(this.props.reconNavStatus)
        {
            case 1:
            component=<Retailer/>
            break;
            case 2:
            component=<Transaction/>
            break;
            case 3:
            component=<Payment/>
            break;
            default:
            component=<Retailer/>
            break;
        }
        return(
                   <Grid fluid>
                       <Row className='reconciliation-showcase'>
                           <Col>{component}</Col>
                       </Row>
                   </Grid>
        );
    }
}

const mapStateToProps=(state)=>{
    return({
        reconNavStatus:state.navigationReducer.reconNav
    })
}

export default connect(mapStateToProps)(Reconciliation);