import React, { Component } from 'react';
import logo from '../images/logo.png';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { sidebarNavigation } from '../action/index';
import { Nav, NavItem, Grid, Row, Col, Glyphicon } from 'react-bootstrap';

class Sidebar extends Component{
    constructor()
    {
        super()
        this.state={select:1};
        this.onSelect=this.onSelect.bind(this);
    }

    onSelect(e){
       this.setState({select:e},function(){
           console.log(this.state)
           this.props.sidebarNavigation(this.state);
       });
    }

    render(){
        return(
            <div className='root-sidebar'>
                  <Grid fluid>
                      <Row className='sidebar-header'>
                          <Col><img src={logo} className='logo'/></Col>
                      </Row>
                  </Grid>
                  <Nav stacked className='sidebar-navigation' bsStyle='pills' activeKey={this.state.select} onSelect={this.onSelect}>
                      <NavItem eventKey={1} >
                          <Glyphicon glyph='list'></Glyphicon>
                          Reconciliation
                      </NavItem>
                      <NavItem eventKey={2} >
                          <Glyphicon glyph='briefcase'></Glyphicon>
                          Catalogue
                      </NavItem>
                  </Nav>

                  
            </div>
        );
    }
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({sidebarNavigation},dispatch)
}

export default connect(null,mapDispatchToProps)(Sidebar);