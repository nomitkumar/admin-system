import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Nav, NavItem, Glyphicon } from 'react-bootstrap';
import { reconciliationNavigation,catalogueNavigation } from '../action/index'; 
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

class Header extends Component{
    constructor(){
        super()
        this.state={select:1};
        this.onSelectReconciliation=this.onSelectReconciliation.bind(this);
        this.onSelectCatalogue=this.onSelectCatalogue.bind(this);
    }

    onSelectReconciliation(eventKey){
        console.log(eventKey)
        this.setState({select:eventKey},function(){
            this.props.reconciliationNavigation(this.state);
        });
    }

    onSelectCatalogue(eventKey){
        this.setState({select:eventKey},function(){
            this.props.catalogueNavigation(this.state)
        })
    }

    componentDidUpdate(prevProps){
        if(prevProps.heading!==this.props.heading)
        this.setState({select:1});
        if(this.props.filterRetailer!==0)
        this.setState({select:2});
    }

    render(){
        let nav;
        switch(this.props.heading)
        {
            case 'Reconciliation':
            nav=<Nav bsStyle='tabs' activeKey={this.state.select} onSelect={this.onSelectReconciliation}>
                   <NavItem eventKey={1}>Retailer</NavItem>
                   <NavItem eventKey={2}>Transaction</NavItem>
                   <NavItem eventKey={3}>Payment</NavItem>
                </Nav>
                break;
            case 'Catalogue':
            nav=<Nav bsStyle='tabs' activeKey={this.state.select} onSelect={this.onSelectCatalogue}>
                    <NavItem eventKey={1}><Glyphicon glyph='globe'></Glyphicon>Global</NavItem>
                    <NavItem eventKey={2}>Retailer</NavItem>
                </Nav> 
                break;
            default:
            break;       
        }
        return(
            <Grid fluid>
                <Row>
                    <Col>
                    <Row className='app-header'>
                        <Col lg={3} md={3} className='header-heading'>{this.props.heading}</Col>
                        <Col lg={9} md={9}>
                          {nav}
                        </Col>
                    </Row>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps=(state)=>{
    return({
        heading:state.navigationReducer.sideNav,
        filterRetailer:state.retailerReducer.pendingId
    });
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({reconciliationNavigation,catalogueNavigation},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);