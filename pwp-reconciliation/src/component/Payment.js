import React, { Component } from 'react';
import PaymentDetailsModal from './PaymentDetailsModal';
import Spinner from './Spinner';
import { connect } from 'react-redux';
import { Table, OverlayTrigger, Tooltip, Pager } from 'react-bootstrap';

class Payment extends Component{
    constructor()
    {
        super()
        this.state={payment:{},open:false,payments:[],id:'1',date:'',spinner:true,name:'',offset:0, nextPage:
        false, prevPage:false
    }
        this.onClick=this.onClick.bind(this);
        this.closeModal=this.closeModal.bind(this);
        this.onClickPagination=this.onClickPagination.bind(this);
    }

    onClickPagination(e){
        console.log(e.target.text);
        switch(e.target.text){
            case 'Previous':
            if(this.state.prevPage){
                this.setState({offset:this.state.offset-15})
            }
            break;
            case 'Next':
            if(this.state.nextPage){
                this.setState({offset:this.state.offset+15})
            }
            break;
        }
    }

    closeModal(){
        this.setState({open:false,payment:{}})
    }

    onClick(item)
    {
       console.log(item)
       this.setState({payment:item,open:true,name:item.transactions[0].retailer.retailerName})
    }

    componentWillMount(){
        fetch('http://35.200.230.50/pwpreconciliation/v1/payment/?limit=15')
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            if(data.message===undefined){
                this.setState({payments:data.result, spinner:false, nextPage:data.hasNextPage, prevPage:data.hasPrevPage})
            }
        })
        .catch(e=>{
            console.log(e)
        })
    }

    componentDidUpdate(prevProps,prevState){
        console.log(this.props.retailerId);
        let startPoint;
         if(this.props.retailerId!==this.state.id||this.props.paymentDate!==this.state.date || prevState.offset!==this.state.offset)
         {
             if(prevState.offset!==this.state.offset){
                 startPoint=this.state.offset;
             }
             if(this.props.retailerId!==this.state.id||this.props.paymentDate!==this.state.date){
                 startPoint=0
             }
             this.setState({id:this.props.retailerId,date:this.props.paymentDate,spinner:true,offset:startPoint,payments:[]},function(){
                 console.log(this.state.id)
                 if(this.state.id==='1'){
                   if(this.state.date===''){
                       fetch('http://35.200.230.50/pwpreconciliation/v1/payment/?limit=15')
                       .then(response=>{
                           return response.json()
                       })
                       .then(data=>{
                           if(data.message===undefined){
                           this.setState({payments:data.result,spinner:false,nextPage:data.hasNextPage,
                            prevPage:data.hasPreviousPage})
                           }
                       })
                   }
                   else{
                       fetch(`http://35.200.230.50/pwpreconciliation/v1/payment/?createOn=${this.state.date}&&offset=${this.state.offset}&&limit=15`)
                       .then(response=>{
                           return response.json()
                       })
                       .then(data=>{
                        if(data.message===undefined){
                        this.setState({payments:data.result,spinner:false,nextPage:data.hasNextPage,
                        prevPage:data.hasPreviousPage})
                        }
                       })
                       .catch(e=>{
                           console.log(e);
                       })
                   }
                 }
                 else{
                     if(this.state.date===''){
                     fetch(`http://35.200.230.50/pwpreconciliation/v1/payment/?retailerCode=${this.state.id}&&limit=15&&offset=${this.state.offset}`)
                     .then(response=>{
                         return response.json()
                     })
                     .then(data=>{
                         console.log(data.result)
                         if(data.message===undefined){
                            this.setState({payments:data.result,spinner:false,nextPage:data.hasNextPage,
                            prevPage:data.hasPreviousPage})
                        }
                     })
                     .catch(e=>{
                         console.log(e)
                     })
                    }
                    else{
                        fetch(`http://35.200.230.50/pwpreconciliation/v1/payment/?retailerCode=${this.state.id}&&createdOn=${this.state.date}&&limit=15&&offset=${this.state.offset}`)
                        .then(response=>{
                            return response.json()
                        })
                        .then(data=>{
                            console.log(data);
                            if(data.message===undefined){
                                this.setState({payments:data.result,spinner:false,nextPage:data.hasNextPage,
                                prevPage:data.hasPreviousPage})
                            }
                        })
                        .catch(e=>{
                             console.log(e);
                        })
                    }
                 }
             })
         }
    }

    render()
    {
        console.log(this.state)
        const tooltipleft = (
            <Tooltip id="tooltip">
              <strong>Newer</strong>
            </Tooltip>
          );
        const tooltipright =(
            <Tooltip id="tooltip">
              <strong>Older</strong>
            </Tooltip>
        )
        let list;
        let paymentList=[];
        paymentList=this.state.payments.sort(function(b,a){
            if(a.createdOn<b.createdOn) return -1;
            if(a.createdOn>b.createdOn) return 1;
            return 0;
        })
        list=paymentList.map(item=>{
            return<tr className='payments-detail'><td className='payment-details-id'><a onClick={()=>this.onClick(item)}>{item._id}<span className='tooltiptext'>Payment Details</span></a></td>
            <td>{item.transactions[0].retailer.retailerName}</td>
            <td className='text-center'>{item.transactions[0].retailer.retailerCode}</td>
            <td className='text-center'>{item.createdOn}</td>
            <td className='text-center'>{item.amount}</td>
            </tr>
        })
        return(
            <div className='payment-showcase'>
            <div className='product-pagination'>
             <Pager>
                    <OverlayTrigger placement="top" overlay={tooltipleft} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.prevPage}>Previous</Pager.Item></OverlayTrigger>{' '}
                    <OverlayTrigger placement='top' overlay={tooltipright} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.nextPage}>Next</Pager.Item></OverlayTrigger>
            </Pager>
            </div>
            <Table hover condensed responsive>
                <thead className='table-heading'>
                    <tr>
                        <th className='payment-id'>Payment</th>
                        <th className='payment-retailername'>Retailer Name</th>
                        <th className='payment-retailercode'>Retailer Code</th>
                        <th className='payment-date'>Date</th>
                        <th className='payment-amount'>Amount</th>
                    </tr>
                </thead>
                {this.state.spinner||this.state.payments.length===0?<tbody></tbody>:<tbody>{list}</tbody>}
            </Table>
            {this.state.spinner?<Spinner/>:this.state.payments.length===0?<h2 classname='no-match-found'></h2>:''}
            <PaymentDetailsModal open={this.state.open} name={this.state.name} payment={this.state.payment} onClose={this.closeModal}/>
            </div>
        )
    }
}


const mapStateToProps=(state)=>{
    return({
        paymentList:state.paymentReducer.payments,
        filterDate:state.transactionReducer.date,
        retailerId:state.paymentReducer.id,
        paymentDate:state.paymentReducer.date
    })
}
export default connect(mapStateToProps)(Payment);