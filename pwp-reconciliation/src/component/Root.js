import React, { Component } from 'react';
import Sidebar from '../component/Sidebar';
import Reconciliation from '../component/Reconciliation';
import Catalogue from './Catalogue';
import PaymentModal from '../component/PaymentModal';
import CatalogueFilter from './CatalogueFilter';
import Filter from './Filter';
import { Grid, Row, Col } from 'react-bootstrap';
import Header from './Header';
import { connect } from 'react-redux';
import { BrowserRouter, Route} from 'react-router-dom';

class Root extends Component{
    render(){
        let component;
        let filter;
        switch(this.props.sideNavStatus)
        {
            case 'Reconciliation':
            component=<Reconciliation/>
            filter=<Filter/>;
            break;
            case 'Catalogue':
            component=<Catalogue/>
            filter=<CatalogueFilter/>
            break;
        }
        return(
           <Grid fluid className='root-showcase'>
               <Row>
                   <Col lg={2} md={2} sm={2} className='column'><Sidebar/></Col>
                   <Col lg={10} md={10} sm={10} className='column'>
                       <Row>
                           <Col><Header/></Col>
                       </Row>
                       <Row className='root-filter'>
                           <Col>{filter}</Col>
                       </Row>
                       <Row className='root-body'>
                           <Col>
                             {component}
                           </Col> 
                       </Row>
                   </Col>
               </Row>
               <PaymentModal/>
           </Grid>
        );
    }
}

const mapStateToProps=(state)=>{
    return({
        sideNavStatus:state.navigationReducer.sideNav
    })
}

export default connect(mapStateToProps)(Root);