import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { productPopup,productPopupClose } from '../action/index';
import { Modal,Button, ButtonToolbar, FormGroup, FormControl, ControlLabel, Grid, Row, Col, Checkbox } from 'react-bootstrap';

class ProductModal extends Component{
    constructor(props){
        super(props)
        this.state={img:'',
                    productname:'',
                    category:'',
                    nickname:'',
                    productType:'',
                    description:'',
                    mrp:'',
                    costPrice:'',
                    sellingPrice:'',
                    cgst:'',
                    sgst:'',
                    igst:'',
                    ugst:'',
                    cess:'',
                    hsnCode:'',
                    offerName:'',
                    offerType:''
                };
        this.onClose=this.onClose.bind(this);
        this.onSave=this.onSave.bind(this);
        this.onSaveVerify=this.onSaveVerify.bind(this);
        this.onChangeImg=this.onChangeImg.bind(this);
        this.onChange=this.onChange.bind(this);
    }

    onChangeImg(e){
        console.log(e.target.value)
        let files=e.target.files
        let reader=new FileReader()
        reader.readAsDataURL(files[0])
        reader.onload=(e)=>{
            console.log('data',e.target.result)
            this.setState({img:e.target.result})
        }
    }

    onChange(e){
        console.log(e.target.getAttribute('data-id'))
        if(e.target.getAttribute('data-id')==='LOOSE'||e.target.getAttribute('data-id')==='PREPACKED'){
             this.setState({productType:e.target.getAttribute('data-id')})
        }
        else{
            this.setState({[e.target.getAttribute('data-id')]:e.target.value})
        }
    }

    onSave(){
        let manipulatedData={
            category: this.state.category,
            description: this.state.description,
            imageUrls:[this.state.img],
            tax: {
            hsnNumber: this.state.hsnCode,
            igst: this.state.igst,
            sgst: this.state.sgst,
            cess: this.state.cess,
            utgst: this.state.utgst,
            cgst: this.state.cgst
            },
            approvalInfo: this.props.productData.approvalInfo,
            productType: this.state.productType,
            typeMeta: {
            barcode: this.props.productData.typeMeta.barcode,
            mrp: this.state.mrp,
            costPrice: this.state.costPrice,
            quantity: this.props.productData.typeMeta.quantity,
            bestBefore:this.props.productData.typeMeta.bestBefore,
            sellingPrice: this.state.sellingPrice
            },
            _id: this.props.productData._id,
            nickname: this.state.nickname,
            name: this.state.productname    
        }
        if(this.props.catNavStatus===1){
            console.log(manipulatedData);
        fetch(`http://35.200.230.50/productcatalog/v1/libraryproduct/${this.props.productData._id}`,{
            method:'PUT',
            headers:{
                'Content-Type': 'application/json'
              },
            body:JSON.stringify(manipulatedData)
        })
        .then(response=>{
            console.log(response.json());
        })
        .catch(e=>{
            console.log(e);
        })
        }
        else{
            manipulatedData={...manipulatedData, cloudProductId: this.props.productData.cloudProductId,retailerCode:this.props.retailerId}
            fetch(`http://35.200.230.50/productcatalog/v1/retailer/23416/product/${this.props.productData._id}`,{
                method:'PUT',
                headers:{
                    'Content-Type': 'application/json' 
                },
                body:JSON.stringify(manipulatedData)
            })
            console.log(manipulatedData)
        }

    this.props.productPopupClose(false)
    }

    onSaveVerify(){
        let manipulatedData={
            category: this.state.category,
            description: this.state.description,
            imageUrls:[this.state.img],
            tax: {
            hsnNumber: this.state.hsnCode,
            igst: this.state.igst,
            sgst: this.state.sgst,
            cess: this.state.cess,
            utgst: this.state.utgst,
            cgst: this.state.cgst
            },
            approvalInfo: {
                    status: "APPROVED",
                    approvedOn: this.props.productData.approvalInfo.approvedOn,
                    approvedBy: this.props.productData.approvalInfo.approvedBy
            },
            productType: this.state.productType,
            typeMeta: {
            barcode: this.props.productData.typeMeta.barcode,
            mrp: this.state.mrp,
            costPrice: this.state.costPrice,
            quantity: this.props.productData.typeMeta.quantity,
            bestBefore:this.props.productData.typeMeta.bestBefore,
            sellingPrice: this.state.sellingPrice
            },
            _id: this.props.productData._id,
            nickname: this.state.nickname,
            name: this.state.productname    
        }
        if(this.props.catNavStatus===1){
            fetch(`http://35.200.230.50/productcatalog/v1/libraryproduct/${this.props.productData._id}`,{
            method:'PUT',
            headers:{
                'Content-Type': 'application/json'
              },
            body:JSON.stringify(manipulatedData)
        })
        .then(response=>{
            console.log(response.json());
        })
        .catch(e=>{
            console.log(e);
        })
        }
        this.props.productPopupClose(false);
    }

    componentDidUpdate(prevProps){
        let product=this.props.productData;
        if(prevProps.productData!==this.props.productData&&this.props.productData!=='')
        {
        this.setState({
        img:product.imageUrls[0],
        productname:product.name,
        category:product.category,
        nickname:product.nickname,
        description:product.description,
        productType:product.productType,
        mrp:product.typeMeta.mrp,
        costPrice:product.typeMeta.costPrice,
        sellingPrice:product.typeMeta.sellingPrice,
        cgst:product.tax.cgst,
        sgst:product.tax.sgst,
        igst:product.tax.igst,
        utgst:product.tax.utgst,
        cess:product.tax.cess,
        hsnCode:product.tax.hsnNumber,
        offerName:'',
        offerType:''
        })
    }
    }

    onClose(){
        this.props.productPopupClose(false);
    }

    render(){
        return(
            <Modal show={this.props.open} className='product-modal'>
                 <Modal.Header>
                     <Modal.Title>
                         Product Details
                     </Modal.Title>
                 </Modal.Header>
                 <Modal.Body>
                        <Grid fluid>
                                 <Row>
                                     <Col lg={8}>
                                         <Row>
                                             <Col>
                                                <FormGroup>
                                                    <ControlLabel>Product Name</ControlLabel>
                                                    <FormControl type='text' value={this.state.productname} data-id='productname' onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                             </Col>
                                         </Row>
                                         <Row>
                                           <Col>
                                                <FormGroup>
                                                    <ControlLabel>Category</ControlLabel>
                                                    <FormControl type='text' value={this.state.category} data-id='category' onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                             </Col>
                                         </Row>
                                         <Row>
                                           <Col>
                                                <FormGroup>
                                                    <ControlLabel>Nickname</ControlLabel>
                                                    <FormControl type='text' value={this.state.nickname} data-id='nickname' onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                             </Col>
                                         </Row>
                                     </Col>
                                     <Col lg={4} className='column text-right'>
                                        <Row>
                                            <Col className='product-image-area text-right'><img src={this.state.img}/></Col>
                                        </Row>
                                        <Row>
                                           <Col><FormControl type='file' className='choose-file' onChange={this.onChangeImg}></FormControl></Col>
                                        </Row>
                                     </Col>
                                 </Row>
                                 <Row>
                                     <Col lg={8}>
                                     <FormGroup controlId="formControlsTextarea">
                                         <ControlLabel>Description</ControlLabel>
                                         <FormControl componentClass="textarea"  value={this.state.description} className='description' data-id='description' onChange={this.onChange}/>
                                     </FormGroup>                                
                                     </Col>
                                     <Col lg={4}>
                                        <FormGroup className='product-modal-checkbox'>
                                            <ControlLabel>Packaged Type</ControlLabel>
                                            <Checkbox checked={this.state.productType==='LOOSE'} data-id='LOOSE' onChange={this.onChange}>Loose</Checkbox>
                                            <Checkbox checked={this.state.productType==='PREPACKED'}data-id='PREPACKED' onChange={this.onChange}>Pre-Packaged</Checkbox>
                                        </FormGroup>
                                     </Col>
                                 </Row>
                                 <Row>
                                     <Col lg={4} md={4}>
                                         <FormGroup>
                                             <ControlLabel>Price Per Kg/Ltr</ControlLabel>
                                             <FormControl type='text' value={this.state.costPrice} data-id='costPrice' onChange={this.onChange}></FormControl>
                                         </FormGroup>
                                     </Col>
                                     <Col lg={4} md={4}>
                                         <FormGroup>
                                             <ControlLabel>{this.state.productType==='LOOSE'?'MRP/Selling Price':'Selling Price'}</ControlLabel>
                                             <FormControl type='text' data-id='sellingPrice' value={this.state.sellingPrice} onChange={this.onChange}></FormControl>
                                         </FormGroup>
                                     </Col >
                                     {this.state.productType==='PREPACKED'?<Col lg={4} lg={4}>
                                         <FormGroup>
                                             <ControlLabel>
                                                 MRP
                                             </ControlLabel>
                                             <FormControl type='text' data-id='mrp' value={this.state.mrp} onChange={this.onChange}></FormControl>
                                         </FormGroup>
                                     </Col>:''}
                                 </Row>
                                 <Row>
                                     <Col>
                                     <Row><Col><h4>TAX</h4></Col></Row>
                                        <Row>
                                            <Col lg={4} md={4}>
                                               <FormGroup>
                                                    <ControlLabel>CGST</ControlLabel>
                                                    <FormControl type='text' data-id='cgst' value={this.state.cgst} onChange={this.onChange}></FormControl>
                                                </FormGroup> 
                                            </Col>
                                            <Col lg={4} md={4}>
                                            <FormGroup>
                                                    <ControlLabel>SGST</ControlLabel>
                                                    <FormControl type='text' data-id='sgst' value={this.state.sgst} onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                            </Col>
                                            <Col lg={4} md={4}>
                                            <FormGroup>
                                                    <ControlLabel>IGST</ControlLabel>
                                                    <FormControl type='text' data-id='igst' value={this.state.igst} onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg={4} md={4}>
                                            <FormGroup>
                                                    <ControlLabel>UTGST</ControlLabel>
                                                    <FormControl type='text' data-id='utgst'value={this.state.utgst} onChange={this.onChange}></FormControl>
                                                </FormGroup> 
                                            </Col>
                                            <Col lg={4} md={4}>
                                            <FormGroup>
                                                    <ControlLabel>CESS</ControlLabel>
                                                    <FormControl type='text' data-id='cess' value={this.state.cess} onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                            </Col>
                                            <Col lg={4} md={4}>
                                            <FormGroup>
                                                    <ControlLabel>HSN Code</ControlLabel>
                                                    <FormControl type='text' data-id='hsnCode' value={this.state.hsnCode} onChange={this.onChange}></FormControl>
                                                </FormGroup>
                                            </Col>
                                        </Row>                                     
                                     </Col>
                                 </Row>
                            </Grid>
                 </Modal.Body>
                 <Modal.Footer>
                     <ButtonToolbar className='product-edit-button'>
                         <Button onClick={this.onClose}>close</Button>
                         <Button bsStyle='primary' onClick={this.onSave}>save</Button>
                         <Button bsStyle='warning' onClick={this.onSaveVerify}>save & verify</Button>
                     </ButtonToolbar>
                 </Modal.Footer>
            </Modal>
        )
    }
}

const mapStateToProps=(state)=>{
    return({
        open:state.catalogueReducer.productPopup,
        productData:state.catalogueReducer.product,
        retailerId:state.productReducer.id,
        catNavStatus:state.navigationReducer.catNav
    })
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({productPopup,productPopupClose},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductModal);