import React, { Component } from 'react';
import { Modal, Grid, Col, Row,Button,Table } from 'react-bootstrap';
import Spinner from './Spinner';

class PaymentDetailsModal extends Component{
    
    render(){
        console.log(this.props.payment)
        let list=[];
        if(this.props.payment.transactions!==undefined){
            list=this.props.payment.transactions.map(item=>{
                return <tr>
                    <td className='text-center'>{item._id}</td>
                    <td className='text-left'>{item.customer.customerName}</td>
                    <td className='text-center'>{item.createdOn}</td>
                    <td className='text-center'>{item.points}</td>
                    <td className='text-center'>{item.discount}</td>
                </tr>
            })
        }
        return(
            <Modal show={this.props.open} className='payment-details-modal'>
              <Modal.Header>
                  <Modal.Title>Payment Description</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              {this.props.payment?<div><Grid fluid>
                    <Row>
                        <Col lg={4} md={4} sm={4}>
                            <Row>
                               <Col lg={6} md={6} sm={6}>Payment Id:</Col>
                               <Col lg={6} md={6} sm={6} className=''>{this.props.payment.paymentId}</Col> 
                            </Row>                    
                        </Col>
                        <Col lg={5} md={5} sm={5}>
                           <Row>
                               <Col lg={6} md={6} sm={6}>Retailer Name:</Col>
                               <Col lg={6} md={6} sm={6}>{this.props.name}</Col>
                           </Row>
                        </Col>
                        <Col lg={3} md={3} sm={3}>
                           <Row>
                               <Col lg={6} md={6} sm={6}>Mode:</Col>
                               <Col lg={6} md={6} sm={6}>{this.props.payment.mode}</Col>
                            </Row>                     
                        </Col>
                    </Row>
                </Grid>
                <Table hover striped responsive condensed>
                    <thead>
                        <tr>
                            <th className='text-center'>Transaction</th>
                            <th className='text-left'>Customer</th>
                            <th className='text-center'>Date</th>
                            <th className='text-center'>Points</th>
                            <th className='text-center'>Discount</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list}
                    </tbody>
                </Table></div>:''}
              </Modal.Body>
              <Modal.Footer>
                  <Button bsStyle='primary' onClick={this.props.onClose}>OK</Button>
              </Modal.Footer>
            </Modal>
        )
    }
}

export default PaymentDetailsModal;