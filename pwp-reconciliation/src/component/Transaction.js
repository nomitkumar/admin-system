import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { makePayment,selectAll } from '../action/index';
import PaymentModal from './PaymentModal';
import Spinner from './Spinner';
import { Grid, Row, Col, Checkbox, Pager, Table, Glyphicon, OverlayTrigger,Tooltip} from 'react-bootstrap';

class Transaction extends Component{
    constructor()
    {
        super()
        this.state={list:[],id:'1',date:'',status:'Status',paymentList:[],selectAll:false,spinner:true,
                    nextPage:false,prevPage:false,offset:0          
    };
        this.onChange=this.onChange.bind(this);
        this.onSelectAll=this.onSelectAll.bind(this);
        this.onClick=this.onClickPagination.bind(this);
      
    }

    onClickPagination(e){
        switch(e.target.text){
            case 'Previous':
            if(this.state.prevPage){
                this.setState({offset:this.state.offset-50})
            }
            break;
            case 'Next':
            if(this.state.nextPage){
                this.setState({offset:this.state.offset+50})
            }
            break;
        }
    }

    onSelectAll(e){
        let status=e.target.checked;
        this.setState({spinner:true},function(){
        if(status){
            fetch(`http://saggregation.mpaani.com/pwpreconciliation/v1/pwptransaction/?retailerCode=${this.state.id}&&status=${this.state.status}&&limit=50&&offset=${this.state.offset}`)
            .then(response=>{
                return response.json()
            })
            .then(data=>{
                let list=data.result.map(item=>{
                    return item._id
                })
                this.setState({paymentList:list,selectAll:true,spinner:false})
            })
            .catch(e=>{
                console.log(e);
            })
        }
        else{
            this.setState({paymentList:[],selectAll:false,spinner:false})
        }
    })
    }

    onChange(e){
        let list;
        if(e.target.checked===false){
            list=this.state.paymentList.filter(item=>{
            if(item===e.target.id)
            return false;
            else
            return true;
        })
        this.setState({paymentList:list},function(){
            if(this.state.paymentList.length===0)
            this.setState({selectAll:false})
        })
        }
        else{
            list=[e.target.id,...this.state.paymentList]
            this.setState({paymentList:list})
        }
    }

    componentWillMount(){
        fetch('http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?limit=50&&offset=0')
        .then(response=>{
            return response.json();
        })
        .then(data=>{
            if(data.message===undefined){
            this.setState({list:data.result,spinner:false,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage})
        }
        })
        .catch(e=>{
            console.log(e);
        })
    }

    componentDidUpdate(prevProps,prevState){
        console.log(this.state)
        let startPoint=0;
        let list=[];
        if(this.props.open!==prevProps.open){
            if(this.props.open)
            this.props.makePayment(this.state.paymentList)
            else
            this.props.makePayment([]);
        }
        if(this.props.retailerDetails.id!==this.state.id||this.props.retailerDetails.status!==this.state.status||
            this.props.retailerDetails.date!==this.state.date||prevState.offset!==this.state.offset
            ){
             if(prevState.offset!==this.state.offset){
                startPoint=this.state.offset;
                list=[...this.state.paymentList];
             }
            this.setState({id:this.props.retailerDetails.id,status:this.props.retailerDetails.status,list:[],
                date:this.props.retailerDetails.date,offset:startPoint, spinner:true,selectAll:false,paymentStatus:false},function(){
                this.setState({paymentList:list})    
                if(this.state.id==='1'){
                    if(this.state.status==='Status'){
                        if(this.state.date===''){
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?limit=50&&offset=${this.state.offset}`)
                            .then(response=>{
                                return response.json();
                            })
                            .then(data=>{
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e);
                            })
                        }
                        else{
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?limit=50&&offset=${this.state.offset}&&createdOn=${this.state.date}`)
                            .then(response=>{
                                return response.json();
                            })
                            .then(data=>{
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e);
                            })
                        }
                    }
                    else{
                        if(this.state.date===''){
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?limit=50&&offset=${this.state.offset}&&status=${this.state.status}`)
                            .then(response=>{
                                return response.json()
                            })
                            .then(data=>{
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e)
                            })
                        }
                        else{
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?limit=50&&offset=${this.state.offset}&&status=${this.state.status}&&createdOn=${this.state.date}`)
                            .then(response=>{
                                return response.json()
                            })
                            .then(data=>{
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e)
                            })
                        }
                    }
                }
                else{
                    if(this.state.status==='Status'){
                        if(this.state.date===''){
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?retailerCode=${this.state.id}&&limit=50&&offset=${this.state.offset}`)
                            .then(response=>{
                                return response.json()
                            })
                            .then(data=>{
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e)
                            })
                        }
                        else{
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?retailerCode=${this.state.id}&&createdOn=${this.state.date}&&limit=50&&offset=${this.state.offset}`)
                            .then(response=>{
                                return response.json()
                            })
                            .then(data=>{
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e)
                            })
                        }
                    }
                    else{
                        if(this.state.date===''){
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?retailerCode=${this.state.id}&&status=${this.state.status}&&limit=50&&offset=${this.state.offset}`)
                            .then(response=>{
                                return response.json()
                            })
                            .then(data=>{
                                console.log(data)
                                if(data.message===undefined){
                                this.setState({list:data.result,nextPage:data.hasNextPage,prevPage:data.hasPreviousPage,spinner:false});
                                }
                            })
                            .catch(e=>{
                                console.log(e)
                            })
                        }
                        else{
                            fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/?retailerCode=${this.state.id}&&status=${this.state.status}&&createdOn=${this.state.date}&&limit=50&&offset=${this.state.offset}`)
                            .then(response=>{
                                return response.json()
                            })
                            .then(data=>{
                                this.setState({list:data.result})
                            })
                            .catch(e=>{
                                console.log(e)
                            })
                        }
                    }
                }
            })
        }
    }
    
    render(){
        console.log(this.state)
        const tooltipleft = (
            <Tooltip id="tooltip">
              <strong>Newer</strong>
            </Tooltip>
          );
        const tooltipright =(
            <Tooltip id="tooltip">
              <strong>Older</strong>
            </Tooltip>
        )

        let select='';
        if(this.props.retailerDetails.paymentOption&&this.state.status!=='PAID')
        select=<Checkbox onChange={this.onSelectAll} checked={this.state.selectAll}></Checkbox>;

        let retailerList=[];
        let transactions=[];
        transactions=this.state.list.sort(function(b,a){
            if(a.createdOn<b.createdOn) return -1;
            if(a.createdOn>b.createdOn) return 1;
            return 0;
        })
        let checkbox;
        let total=0;
        let filterData= this.props.retailerDetails;
         retailerList=transactions.map(item =>{
             if(item.status==='PENDING')
             total=total+item.discount;
            if(filterData.paymentOption===true &&this.props.reconStatus===2)
            {
            if(item.status==='PENDING'){
            checkbox=<Checkbox id={item._id} onChange={this.onChange} checked={this.state.paymentList.includes(item._id)}></Checkbox>
            }
            }
            else
            {
               if(item.status==='PAID')
               checkbox='Paid';
               else
               checkbox='Pending'; 
            }
            console.log(item._id)
            return <tr key={item._id}>
            <td className='transaction-status'>{checkbox}</td> 
            <td className='transaction-retailer'>{item.retailer.retailerName}</td>
            <td className='transaction-retailercode'>{item.retailer.retailerCode}</td>
            <td className='transaction-date'>{item.createdOn}</td>
            <td className='transaction-points'>{item.points}<span className='tooltiptext'>
                <Grid fluid>
                    <Row>
                        <Col className='column text-left'>TransactionId : {item._id}</Col>
                        <Col className='column text-left'>Customer : {item.customer.customerName}</Col>
                        <Col className='column text-left'>CustomerId : {item.customer.customerId}</Col>
                    </Row>
                </Grid>
            </span></td>
            <td className='transaction-discount'>{item.discount}</td>
            </tr>
        })
        console.log(retailerList)
        return(
            <div>
            <Grid fluid>
                <Row className='total'>
                    <Col lg={4} md={4} sm={4} className='select-all text-left'>{filterData.paymentOption&&this.state.status!=='PAID'?'Select All':''}<span>{select}</span></Col>
                    <Col lg={4} md={4} sm={4} className='pagination'>
                    <Pager>
                    <OverlayTrigger placement="top" overlay={tooltipleft} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.prevPage}>Previous</Pager.Item></OverlayTrigger>{' '}
                    <OverlayTrigger placement='top' overlay={tooltipright} delayShow={300} delayHide={150}><Pager.Item onClick={this.onClickPagination} disabled={!this.state.nextPage}>Next</Pager.Item></OverlayTrigger>
                    </Pager>
                    </Col>
                    <Col lg={2} md={2}></Col>
                    <Col lg={2} md={2} sm={2} className='total-value text-center'>{this.state.id==='1'||this.state.status!=='PENDING'?'':total}</Col>
                    
                </Row>
            </Grid>
            <Table hover condensed striped responsive>
                  <thead className='table-heading'>
                      <tr>
                          <th className='transaction-status'>Status</th>
                          <th className='transaction-retailer'>Retailer</th>
                          <th className='transaction-retailercode'>Retailer Code</th>
                          <th className='transaction-date'>Date</th>
                          <th className='transaction-points'><Glyphicon glyph='tint'>Points</Glyphicon></th>
                          <th className='transaction-discount'>Discount</th>
                      </tr>
                  </thead>
                  {this.state.spinner||this.state.list.length===0?<tbody></tbody>:<tbody>{retailerList}</tbody>}          
            </Table>
                  {this.state.spinner?<Spinner/>:this.state.list.length===0?<h2 className='no-match-found'>No result found!..</h2>:''}
            <PaymentModal/>
            </div>
        );
    }
}

const mapStateToProps=(state)=>{
    return({retailerDetails:state.transactionReducer,
        open:state.paymentReducer.modalOpen,
        reconStatus:state.navigationReducer.reconNav})
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({makePayment,selectAll},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Transaction);