import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { transactionFilter,openPaymentModal,retailerFilter,paymentFilter, retailerPendingTransactionFilter } from '../action/index';
import SelectSearch from 'react-select-search';
import { Grid, Row, Col, FormControl, InputGroup, FormGroup, Glyphicon, Button } from 'react-bootstrap';

class Filter extends Component{
    constructor()
    {
        super()
        this.state={id:'1',status:'Status',date:'',retailers:[],name:'',total:0}
        this.onChange=this.onChange.bind(this);
        this.onSelectRetailer=this.onSelectRetailer.bind(this);
        this.onSelectStatus=this.onSelectStatus.bind(this);
        this.onClick=this.onClick.bind(this);
    }

    onChange(e){
        let d=new Date(e.target.value);
        this.setState({date:e.target.value?d.getTime():''},function(){
            switch(this.props.reconStatus){
                case 2:
                this.props.transactionFilter(this.state);
                break;
                case 3:
                this.props.paymentFilter(this.state.id,this.state.date)
                break;
            }
        })
    }

    onSelectStatus(e)
    {
        this.setState({status:e.target.value},function(){
            this.props.transactionFilter(this.state);
        });
    }

    onSelectRetailer(e){
        this.setState({id:e.value,name:e.name},function(){
            switch(this.props.reconStatus)
            {
                case 1:
                this.props.retailerFilter(this.state.id)
                break;
                case 2:
                this.props.transactionFilter(this.state)
                break;
                case 3:
                this.props.paymentFilter(this.state.id,this.state.date)
                break;

            }
        })
    }


    onClick()
    {
        this.props.openPaymentModal();
    }

    componentWillMount(){
        fetch('http://saggregation.mpaani.com/pwpreconciliation/v1/eligibleretailer/')
        .then(response=>{
            return(response.json())
        })
        .then(data=>{
            if(data.message===undefined){
            this.setState({retailers:data.result,total:data.totalPending})
            }
        })
        .catch(e=>{
            console.log(e);
        })
    }
    
    componentDidUpdate(prevProps){
        console.log('filter')
        if(this.props.reconStatus!==prevProps.reconStatus){
            this.setState({id:'1',name:'',date:'',status:'Status'},function(){
                if(this.props.filterRetailer!==0){
                    this.setState({id:this.props.filterRetailer,name:'',date:'',status:'PENDING'},function(){
                        this.props.transactionFilter(this.state);
                        this.props.retailerPendingTransactionFilter(0);
                    })
                }
                else{
                    this.props.transactionFilter(this.state);
                }
                this.props.retailerFilter(this.state.id);
                this.props.paymentFilter(this.state.id,this.state.date);
            })
        }
        if(this.props.filter.paymentStatus===true&&this.props.filter.paymentStatus!==prevProps.filter.paymentStatus){
            this.setState({status:'PAID'},function(){
                this.props.transactionFilter(this.state);
            })
        }
    }
    
    render(){
        console.log(this.state)
        let options=[{value:'1',name:'Choose Retailer'}]
        if(this.state.retailers.length!==0){
        this.state.retailers.forEach(item=>{
            options.push({value:item.retailerCode,name:item.retailerName})
        })
        }
        return(
          <Grid fluid className='reconciliation-filter'>
              <Row>
                  <Col lg={2} md={2} sm={2}>
                    <SelectSearch options={options} value={this.state.id} onChange={this.onSelectRetailer}/>
                  </Col>
                  <Col className='text-right' lg={2} md={2} sm={2}>
                   {this.props.reconStatus===1||this.props.reconStatus===3?'':
                     <form className='select-form'>
                          <FormGroup controlId='formControlsSelect'>
                              <FormControl componentClass='select' placeholder='Status' onChange={this.onSelectStatus} value={this.state.status}>
                                  <option value='Status'>Status</option>
                                  <option value='PENDING'>Pending</option>
                                  <option value='PAID'>Paid</option>
                            </FormControl>
                          </FormGroup>
                      </form>}
                  </Col>
                  <Col className='text-right' lg={2} md={2} sm={2}>
                      {this.props.reconStatus===1?'':
                      <form>
                          <FormGroup className='date-form'>
                              <InputGroup bsSize='small' className='date-input'>
                                   <InputGroup.Addon>
                                    <Glyphicon glyph='calendar'></Glyphicon>
                                   </InputGroup.Addon>
                                   <FormControl type='date'onChange={this.onChange}/>
                              </InputGroup>
                          </FormGroup>
                      </form>
                      }
                  </Col>
                  <Col lg={4} md={4} sm={4} >
                      <Row className='total-pending'>
                          <Col lg={3} md={3} sm={3} className='column text-right'>
                          TOTAL
                          </Col>
                          <Col lg={9} md={9} sm={9}className='pending-value'>
                          {this.state.total}<span className='tooltiptext'>Total Pending Amount</span>
                          </Col>
                      </Row>
                  </Col>
                  <Col lg={2} md={1} sm={1} className='button text-right'>{this.props.filter.paymentOption&&this.props.reconStatus===2&&this.props.filter.status!=='PAID'?<Button bsStyle='warning'onClick={this.onClick}>Pay Here</Button>:''}</Col>
              </Row>
          </Grid>
        );
    }
}

const mapStateToProps=(state)=>{
    return({
        filter:state.transactionReducer,
        reconStatus:state.navigationReducer.reconNav,
        sidebarStatus:state.navigationReducer.sideNav,
        filterRetailer:state.retailerReducer.pendingId,
    })
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({transactionFilter,openPaymentModal,retailerFilter,paymentFilter, retailerPendingTransactionFilter},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter);