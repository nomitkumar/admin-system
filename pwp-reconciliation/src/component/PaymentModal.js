import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closePaymentModal, paymentMadeSuccessful} from '../action/index';

import { Modal, Grid, Row, Col, FormControl, FormGroup, Button } from 'react-bootstrap';

class PaymentModal extends Component{
    constructor(props){
        super(props)
        this.state={mode:'BANKTRANSFER',paymentId:'',id:'',name:'',total:0,amount:0}
        this.onSave=this.onSave.bind(this);
        this.onChange=this.onChange.bind(this);
        this.onSelect=this.onSelect.bind(this);
        this.onClose=this.onClose.bind(this);
    }

    onChange(e){
        this.setState({paymentId:e.target.value});
    }

    onSelect(e)
    {
        this.setState({mode:e.target.value});
    }

    onClose(){
        this.setState({mode:'BANKTRANSFER',paymentId:'',id:'',name:'',total:0,amount:0},function(){
            this.props.closePaymentModal();
        });
       
    }    

    onSave(){
        let paymentData={
            'retailerCode':this.state.id,
            'pwpTransactionIds':this.props.paymentDetails,
            'mode':this.state.mode,
            'amount':this.state.amount,
            'paymentId':this.state.paymentId
        }
            fetch('http://35.200.230.50/pwpreconciliation/v1/payment/',{
                method:'POST',
                headers:{
                    'Content-Type': 'application/json'
                  },
                body:JSON.stringify(paymentData)
            })
            .then(response=>{
                console.log(response);
            })
            .catch(e=>{
                console.log(e);
            })
            console.log(this.state)
            this.props.closePaymentModal();
            this.props.paymentMadeSuccessful(true)
    }

    componentDidUpdate(prevProps){
        console.log(this.props.paymentDetails)
        if(this.props.paymentDetails.length!==0&&this.props.paymentDetails!==prevProps.paymentDetails){
               console.log(this.props.paymentDetails);
               let total=0;
               let count=0;
           this.props.paymentDetails.forEach(item=>{
               fetch(`http://35.200.230.50/pwpreconciliation/v1/pwptransaction/${item}`)
               .then(response=>{
                   return response.json()
               })
               .then(data=>{
                   console.log(data);
                   total=total+data.discount;
                   count=count+1;
                   if(this.props.paymentDetails.length===count){
                    this.setState({id:data.retailer.retailerCode,name:data.retailer.retailerName,total:count,amount:total})
                   }
               })
               .catch(e=>{
                   console.log(e);
               })
           })
        }
    }

    render(){
        console.log(this.state);
        let onPay=this.state;
        let status=onPay.name===''?true:false;
        return(
            <Modal show={this.props.open}>
                <Modal.Header>
                    <Modal.Title>
                        Make Payment
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Grid fluid className='payment-modal'>
                        <Row>
                            <Col>
                               <Row className='modal-row'>
                                   <Col lg={8} md={8} xs={8} sm={8}>Retailer</Col>
                                   <Col lg={4} md={4} xs={4} sm={4}>{this.state.name}</Col>
                               </Row>
                               <Row className='modal-row'>
                                   <Col lg={8} md={8} xs={8} sm={8}>Retailer Code</Col>
                                   <Col lg={4} md={4} xs={4} sm={4}>{onPay.id}</Col>
                               </Row>
                               <Row className='modal-row'>
                                   <Col lg={8} md={8} xs={8} sm={8}>Total Transaction</Col>
                                   <Col lg={4} md={4} xs={4} sm={4}>{onPay.total}</Col>
                               </Row>
                               <Row className='modal-row'>
                                   <Col lg={8} md={8} xs={8} sm={8}>Amount</Col>
                                   <Col lg={4} md={4} xs={4} sm={4}>{onPay.amount}</Col>
                               </Row>
                               <Row className='modal-row'>
                                   <Col lg={8} md={8} xs={8} sm={8}>Mode</Col>
                                   <Col lg={4} md={4} xs={4} sm={4} className='text-right'>
                                     <form>
                                         <FormGroup controlId='formControlsSelect'>
                                             <FormControl componentClass='select' onChange={this.onSelect}>
                                                 <option value='BANKTRANSFER'>Bank Transfer</option>
                                                 <option value='POINTSCREDITED'>Points Credited</option>
                                             </FormControl>
                                         </FormGroup>
                                     </form>
                                   </Col>
                               </Row>
                               <Row className='modal-row'>
                                   <Col lg={8} md={8} xs={8} sm={8}>Payment Id</Col>
                                   <Col lg={4} md={4} xs={4} sm={4} className='text-right'>
                                       <form>
                                           <FormGroup>
                                               <FormControl type='text' onChange={this.onChange}></FormControl>
                                           </FormGroup>
                                       </form>                                
                                   </Col>
                               </Row>
                               <Row className='modal-button'>
                                   <Col lg={10} md={10} sm={10} xs={10} className='text-right'><Button onClick={this.onClose}>Close</Button></Col>
                                   <Col lg={2} md={2} xs={2} sm={2} ><Button bsStyle='primary' disabled={status} onClick={this.onSave}>Save</Button></Col>
                               </Row>
                               <Row className='modal-row-warning'><Col>{status?'Choose Atleast One Transaction':''}</Col></Row>
                            </Col>
                        </Row>
                    </Grid>
                </Modal.Body>
            </Modal>
        )
        return(
            <div></div>
        )
    }
}

const mapStateToProps=(state)=>{
    return({paymentDetails:state.transactionReducer.paymentList,open:state.paymentReducer.modalOpen});
}

const mpaDispatchToProps=(dispatch)=>{
    return bindActionCreators({closePaymentModal,paymentMadeSuccessful},dispatch)
}

export default connect(mapStateToProps, mpaDispatchToProps)(PaymentModal);