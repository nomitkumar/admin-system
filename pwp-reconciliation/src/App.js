import React, { Component } from 'react';
import { BrowserRouter, Route} from 'react-router-dom';
import Root from './component/Root';
import Reconciliation from './component/Reconciliation';
import catalogue from './component/Catalogue';
import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className='App'>
        <Root/>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
