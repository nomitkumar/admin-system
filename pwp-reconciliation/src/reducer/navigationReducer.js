const initialState={sideNav:'Reconciliation',reconNav:1,catNav:1};

const navigationReducer=(state=initialState,action)=>{
    switch(action.type)
    {
        case 'SIDEBAR_NAVIGATION':
        let option;
        switch(action.payload)
        {
            case 1:
            option='Reconciliation';
            break;
            case 2:
            option='Catalogue';
            break;
            case 3:
            option='';
            break;
            default:
            break;
        }
        state={
            ...state,
            sideNav:option,
            reconNav:1,
            catNav:1
        };
        break;
        case 'RECONCILIATION_NAVIGATION':
        state={
            ...state,
            reconNav:action.payload
        };
        break;
        case 'CATALOGUE_NAVIGATION':
        state={
            ...state,
            catNav:action.payload
        }
        break;
        default:
        break;
    }
    console.log(state);
    return state;
}

export default navigationReducer;