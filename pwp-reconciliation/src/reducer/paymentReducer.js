const initialState={modalOpen:false,payments:[],id:'1',date:''}

const paymentReducer=(state=initialState,action)=>{
    switch(action.type)
    {
        case 'FILTER_PAYMENT':
        state={
            ...state,
            id:action.payload.id,
            date:action.payload.date
        }
        return state;
        case 'OPEN_PAYMENT_MODAL':
        state={...state,
        modalOpen:action.payload
        }
        return state;
        case 'CLOSE_PAYMENT_MODAL':
        state={
            ...state,
        modalOpen:action.payload
        }
        return state;
        default:
        return state;
    }
}

export default paymentReducer;