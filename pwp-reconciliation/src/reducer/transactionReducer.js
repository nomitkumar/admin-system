const initialState={id:'1',name:'',status:'Status',date:'', paymentOption:false, paymentList:[],paymentStatus:false};

const transactionReducer=(state=initialState,action)=>{
    switch(action.type)
    {
        
        case 'FILTER_TRANSACTION':
        let data=action.payload
        state={
            ...state,
            id:data.id,
            status:data.status,
            date:data.date,
            paymentOption:(data.id!=='1'&&data.status==='PENDING')?true:false,
            paymentStatus:false
        }
        console.log(state);
        return state;
        case 'MAKE_PAYMENT':
        state={
            ...state,
            paymentList:action.payload
        }
        console.log(state);
        return state;
        case 'PAYMENT_MADE_STATUS':
        state={
            ...state,
            paymentList:[],
            paymentStatus:true
        }
        return state;
        default:
        return state;
    }
}

export default transactionReducer;