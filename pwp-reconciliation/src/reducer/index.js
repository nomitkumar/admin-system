import { combineReducers } from 'redux';
import paymentReducer from '../reducer/paymentReducer';
import navigationReducer from '../reducer/navigationReducer';
import transactionReducer from '../reducer/transactionReducer';
import catalogueReducer from '../reducer/catalogueReducer';
import retailerReducer from '../reducer/retailerReducer';
import productReducer from './productReducer';

const allReducer = combineReducers({transactionReducer,paymentReducer,navigationReducer,catalogueReducer,retailerReducer,productReducer});

export default allReducer;
