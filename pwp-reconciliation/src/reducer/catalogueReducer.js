const initialState={productPopup:false,product:''}

const catalogueReducer=(state=initialState,action)=>{
    switch(action.type)
    {
        case 'OPEN_PRODUCT_EDIT_POPUP':
        state={
            ...state,
            productPopup:action.payload.status,
            product:action.payload.productData
        }
        break;
        case 'CLOSE_PRODUCT_POPUP':
        state={
            ...state,
            productPopup:action.payload,
            product:''
        }
    }
    console.log(state);
    return state;
}

export default catalogueReducer;