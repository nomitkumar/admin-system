export const makePayment=(data)=>{
    console.log(data);
    return({
        type:'MAKE_PAYMENT',
        payload:data
    })
}

export const openPaymentModal=()=>{
    return({
        type:'OPEN_PAYMENT_MODAL',
        payload:true
    })
}

export const closePaymentModal=()=>{
    return({
        type:'CLOSE_PAYMENT_MODAL',
        payload:false
    })
}


export const reconciliationNavigation=(key)=>{
    return({
        type:'RECONCILIATION_NAVIGATION',
        payload:key.select
    })
}

export const sidebarNavigation=(key)=>{
    return({
        type:'SIDEBAR_NAVIGATION',
        payload:key.select
    })
}

export const catalogueNavigation=(key)=>{
    return({
        type:'CATALOGUE_NAVIGATION',
        payload:key.select
    })
}

export const productPopup=(data,status)=>{
    return({
        type:'OPEN_PRODUCT_EDIT_POPUP',
        payload:{productData:data,status:status}
    })
}
export const selectAll=(status)=>{
    return({
        type:'SELECT_ALL_TRANSACTION',
        payload:status
    })
}
export const productPopupClose=(status)=>{
    return({
        type:'CLOSE_PRODUCT_POPUP',
        payload:status
    })
}

















export const retailerFilter=(id)=>{
    console.log(id);
    return({
        type:'FILTER_RETAILER',
        payload:id
    })
}
export const retailerPendingTransactionFilter=(id)=>{
    console.log(id)
    return({
        type:'FILTER_RETAILER_PENDING_TRANSACTION',
        payload:id
    })
}
export const transactionFilter=(data)=>{
    console.log(data);
    return({
        type:'FILTER_TRANSACTION',
        payload:data
    });
}
export const paymentFilter=(id,date)=>{
    return({
        type:'FILTER_PAYMENT',
        payload:{id:id,date:date}
    })
}
export const productFilter=(data)=>{
    console.log(data);
    return({
        type:'FILTER_PRODUCT',
        payload:{name:data.name,status:data.status,id:data.id}
    })
}

export const paymentMadeSuccessful=(status)=>{
    return({
        type:'PAYMENT_MADE_STATUS',
        payload:status
    })
}
